// Alex Free, Benjamin Fox -- Project 4a
// Dictionary class implementation
// Contains functions to read, sort, and search the dictionary
// Also contains overloaded print operator and getLength function

#include "dictionary.h"

/*
Dictionary constructor reads dictionary words
*/
dictionary::dictionary()
{
    readWords();
}

/*
Opens dictionary.txt and reads words into a vector
*/
void dictionary::readWords()
{
    ifstream fileIn;
    fileIn.open("dictionary.txt");
    if(!fileIn)
    {
        cerr << "The dictionary file does not exit" << endl;
        exit(1);
    }

    string word;
    while(fileIn >> word)
    {
        dict.push_back(word);
    }
    fileIn.close();
}

/*
Prints whole dictionary, one word per line
*/
ostream& operator<< (ostream& ostr, const dictionary& d)
{
    for (int i = 0; i < d.dict.size(); i++)
    {
        ostr << d.dict.at(i) << endl;
    }
    return ostr;
}

/*
Uses selection sort to sort dictionary vector
*/
void dictionary::selsort()
{
    // Repeat sort for 
	for (int i = 0; i < dict.size() - 1; i++)
    {
    	// Print sorting progress for user
        if(i % 1000 == 0)
            cout << i << "/" << dict.size() << endl;

		int minValIndex = i;

		 // Find next alphabetical value in dictionary 
        for (int j = i+1; j < dict.size(); j++)
        {
            if (dict.at(j).compare(dict.at(minValIndex)) < 0)
            {
                minValIndex = j;
            }
        }
        
    	// Swap minVal with current value at i 
        if (minValIndex != i)
        {
            exchange(i, minValIndex);
        }
    }
}

/*
Use binary search to find key in sorted dictionary
*/
int dictionary::search(string key, int lowBound, int upBound) const
{
	
    int mid = (upBound + lowBound)/2; // Find mid value

    if (lowBound > upBound)	// If the lower bound and upper bound have swapped, return -1 (not found)
        return -1;

	// Either return mid or search appropriate half of remaining dictionary
    if (key.compare(dict.at(mid)) == 0)
        return mid;
    else if (key.compare(dict.at(mid)) < 0)
        return search(key, lowBound, mid - 1);
    else if (key.compare(dict.at(mid)) > 0)
        return search(key, mid + 1, upBound);

    return -1;
}

/*
Return the length of the dictionary vector (number of words)
*/
int dictionary::getLength() const
{
    return dict.size();
}

/*
Recursive quick sort algorithm
*/
void dictionary::quicksort(int left, int right)
{
    if (left < right)
    {
        int s = partition(left, right);	// Partition the list and find the partition point
        quicksort(left, s-1); 			// Sort the left side of the list
        quicksort(s+1, right);			// Sort the right side of the list
    }
}

/*
Helper partition function for quick sort
Puts lower numbers to left of pivot, higher numbers to right
Returns pivot index
*/
int dictionary::partition(int l, int r)
{
    string x = dict.at(r);
    int i = l-1;

    for (int j = l; j < r; j++)
    {
        if (dict.at(j).compare(x) <= 0)
        {
            i = i + 1;
            exchange(i, j);
        }
    }
    exchange(i+1, r);
    return i + 1;
}

/*
Helper function that exhanges two indices in the dict vector
*/
void dictionary::exchange(int index1, int index2)
{
    string temp = dict.at(index1);
    dict.at(index1) = dict.at(index2);
    dict.at(index2) = temp;
}

/*
Heap sort that creates, initializes, and sorts heapsort
*/
void dictionary::heapsort()
{
    heap<string> h;
    h.initializeMaxHeap(dict);
    dict = h.heapsort();
    dict.erase(dict.begin());	// Gets rid of first element in vector (heap vector has numbers starting at 1)
}
    



