// Alex Free, Benjamin Fox -- Project 3b
// Main file
// Contains the main function, as well as the functions findWords and findMatches
// The main function calls findMatches, which calls findWords for each of the "directions"

#include "grid.h"
#include "dictionary.h"
#include <stdlib.h>
#include <chrono>

using namespace std;

/*
*findWords takes in a dictionary, a grid, amd integers which give the initial y and x position
*and the "direction" (+1/-1) that the positions will be incremented in.
*it forms an initial 5-character word, and then enters a loop where it searches the dictionary
*for the potential word, prints it if found, and then adds another character to the candidate word.
*/
void findWords(const dictionary &d, const grid &g, int yPos, int xPos, int yInc, int xInc)
{
    //Declares the candidate word
    string word;

    //Form the initial 5-character candidate
    for(int i = 0; i < 5; i++)
    {
        //Gets the starting position
        int y = (yPos + yInc*i)%g.getRows();
        int x = (xPos + xInc*i)%g.getColumns();

        //Wrap around
        if (y < 0)
            y += g.getRows();
        if (x < 0)
            x += g.getColumns();

        //Add the character to candidate word
        word += g.getValue(y, x);
    }

    for(int i = 5; i < g.getRows(); i++){
        //Search the dictionary for the candidate word
        int index = d.search(word, 0, d.getLength() - 1);

        //If the word exists print the word, 
		// print its index and the position of it's starting character in the grid.
       	if (index != -1)
    	{
            cout << word << " at " << index << " in dictionary, and (" << yPos << ", " << xPos << ") in grid" << endl;
       	}
       
        // Get starting position
        int y = (yPos + yInc*i)%g.getRows();
        int x = (xPos + xInc*i)%g.getColumns();

        // Wrap around
        if (y < 0)
            y += g.getRows();
        if (x < 0)
            x += g.getColumns();
            
        //Add next character
        word += g.getValue(y, x);
    }
}

/*
*findMatches takes in a dictionary and a grid object,
*it calls findWords for each of the possible starting characters
*and directions in the list
*/
void findMatches(const dictionary &d, const grid &g)
{
    //account for every position in the grid
    for (int y = 0; y < g.getRows(); y++)
    {
        for (int x = 0; x < g.getColumns(); x++)
        {
            //account for all 8 directions
            for(int yInc = -1; yInc <= 1; yInc++)
            {
                for(int xInc = -1; xInc <= 1; xInc++)
                {
                    //ignore the case where both are zero (not a direction)
                    if(xInc == 0 && yInc == 0)
                        continue;

                    findWords(d, g, y, x, yInc, xInc);
                }
            }
        }
    }
}

/*
Search gets a grid name and sorting algorithm from the user
The sorting time is recorded and dictionary matches in the grid are found
*/
void search()
{
	// Ask user for grid filename and dictionary sorting algorithm type
    string file;
    int sortType;
    cout << "Enter the grid filename" << endl;
    cin >> file;
    cout << "Choose an algorithm \n 0 - selection sort \n 1 - quick sort \n 2 - heap sort \n Enter your choice: ";
    cin >> sortType;

	// Initialize a grid and dictionary object
    grid g(file);
    dictionary d;

	// Print that the dictionary is being sorted
    cout << "Sorting dictionary, please wait..." << endl;

	// Get the time at the start of sorting the dictionary
    auto start = chrono::system_clock::now();
    
    // Choose the right type of sort based on user input
    // If the user chooses an invalid sort type, program will exit
    switch (sortType)
    {
        case 0:
            d.selsort();
            break;
        case 1:
            d.quicksort(0, d.getLength() - 1);
            break;
        case 2:
            d.heapsort();
            break;
        default:
            cout << "Error: " << sortType << " not a valid sort option";
            exit(1);
    }
    
    // Get time at end of dictinoary sorting
    auto end = chrono::system_clock::now();

	// Get time of sorting in seconds
    chrono::duration<double> elapsed_seconds = end - start;

	// Find words in the dictionary that appear in the grid
    findMatches(d, g);

	// Print the sorting time of the dictioanry
    cout << "Time to sort: " << elapsed_seconds.count() << " seconds" << endl;
}

/*
Main calls search to run the program
*/
int main()
{
    search();
}
