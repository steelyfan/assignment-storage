// Alex Free, Benjamin Fox -- Project 3b
// Heap template class
// Contains the functions and data structures necessary for heapsort
#ifndef HEAP_H
#define HEAP_H

#include <math.h>
#include <vector>
using namespace std;

//class declaration
template <typename T>
class heap
{
    public:
    heap();
    vector<T>& heapsort();
    void initializeMaxHeap(const vector<T>& v);

    private:
    int parent(int node);
    int left(int node);
    int right(int node);
    T getItem(int node);
    void maxHeapify(int i);
    void buildMaxHeap();

    vector<T> heap_v;
    int heapsize;
};

//implementation

/*
*Constructor for the heap class
*Initializes heap's vector
*/
template <typename T>
heap<T>::heap()
{
    vector<T> heap_v;

}

/*
*Heapsort calls builMaxHeap, then loops through the heap,
*swapping each node with the top, deincrementing the heapsize
*and calling maxHeapify on the swapped top node
*It returns the sorted vector.
*/
template <typename T>
vector<T>& heap<T>::heapsort()
{
    buildMaxHeap();
    for(int i = heapsize; i > 1; i--)
    {
        T temp =  heap_v.at(1);
        heap_v.at(1) = heap_v.at(i);
        heap_v.at(i) = temp;

        heapsize--;
        maxHeapify(1);
    }
    return heap_v;
}

/*
*parent returns the position of the parent of a given node
*/
template <typename T>
int heap<T>::parent(int node)
{
    return floor(node/2);

}

/*
*left returns the position of the left child of a given node
*/
template <typename T>
int heap<T>::left(int node)
{
    return node * 2;
}

/*
*right returns the position of the right child of a given node
*/
template <typename T>
int heap<T>::right(int node)
{
    return node * 2 + 1;
}

/*
*getItem returns the value contained at a certain node.
*/
template <typename T>
T heap<T>::getItem(int node)
{
    return heap_v.at(node-1);
}

/*
*initializeMaxHeap takes in a vector reference and assigns it to the heap's vector
*it uses a for loop to account for the "shift" (first index starts at 1) that
*heapsort requires
*/
template <typename T>
void heap<T>::initializeMaxHeap(const vector<T>& v)
{
    T place = T();
    heap_v.push_back(place);
    for(int i = 0; i < v.size(); i++)
    {
        heap_v.push_back(v.at(i));
    }
    heapsize = v.size();
    buildMaxHeap();
}

/*
*maxHeapify recursively ensures that the nodes at and below a given node's index
*obey the structure of a maxHeap
*/
template <typename T>
void heap<T>::maxHeapify(int i)
{
    int l = left(i);
    int r = right(i);
    int largest = i;

    if (l <= heapsize && heap_v.at(l) > heap_v.at(largest))
        largest = l;
    if (r <= heapsize && heap_v.at(r) > heap_v.at(largest))
        largest = r;

    if (largest != i)
    {
        T temp =  heap_v.at(i);
        heap_v.at(i) = heap_v.at(largest);
        heap_v.at(largest) = temp;

        maxHeapify(largest);
    }

}

/*
*buildMaxHeap turns the heap's vector into a maxHeap structure
*/
template <typename T>
void heap<T>::buildMaxHeap()
{
    heapsize = heap_v.size() - 1;
    for (int i = floor(heap_v.size()/2); i > 0; i--)
    {
        maxHeapify(i);
    }
}



#endif
