// Alex Free, Benjamin Fox -- Project 3b
// Header file for dictionary contains all function declarations and member variables

#ifndef DICT_H
#define DICT_H

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "heap.h"

using namespace std;

class dictionary
{
    public:
    dictionary();
    void readWords();
    friend ostream& operator<< (ostream& ostr, const dictionary& d);
    void selsort();
    int search(string key, int lowBound, int upBound) const;
    int getLength() const;
    void quicksort(int left, int right);
    void heapsort();

    private:
    int partition(int l, int r);
    void exchange(int index1, int index2);
    vector<string> dict;
};

#endif
