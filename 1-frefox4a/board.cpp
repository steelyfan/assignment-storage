// Alex Free, Benjamin Fox -- Project 4a
// Board class implementation

#include <iostream>
#include <limits.h>
#include "d_matrix.h"
#include "d_except.h"
#include "board.h"
#include <list>
#include <fstream>

using namespace std;


/*
Board constructor initializes value, row, col, and square matrices
Clears board
*/
board::board(int sqSize)
   : value(BoardSize+1,BoardSize+1),
     row(BoardSize+1,BoardSize+1, false),
     col(BoardSize+1,BoardSize+1, false),
     square (BoardSize+1,BoardSize+1, false)
{

   clear();
}

/*
Mark all cells as blank to clear the board
*/
void board::clear()
{
   for (int i = 1; i <= BoardSize; i++)
      for (int j = 1; j <= BoardSize; j++)
      {
         value[i][j] = Blank;
      }
}

/*
Initialize the board with values from a file stream
*/
void board::initialize(ifstream &fin)
{
   char ch;

   clear(); // Clear board

   for (int i = 1; i <= BoardSize; i++)
      for (int j = 1; j <= BoardSize; j++)
	    {
	       fin >> ch; // Read a character by row then column

          // If the read char is not Blank
	      if (ch != '.')
             addCellVal(i,j,ch-'0');   // Convert char to int
        }
}

/*
Initialize the row, col, and square conflict arrays for a starting board
*/
void board::initializeConflicts()
{
	// Loop by every row and column
   	for (int i = 1; i <= BoardSize; i++)
   	{
      for (int j = 1; j <= BoardSize; j++)
      {
      		// Only place conflicts if the current cell is not blank
            if(!isBlank(i, j))
            {
            	// Retreive the cell value
                int cellVal = getCell(i, j);

                // Set the row and column conflicts of the cell value to true
                row[i][cellVal] =  true;
                col[j][cellVal] = true;

                // Set the correct square conflict of the cell value to true
                int squareNum = squareNumber(i, j);
                square[squareNum][cellVal] = true;
            }
        }
    }
}

/*
Return the square number of cell i,j (counting from left to right,
top to bottom.  Note that i and j each go from 1 to BoardSize
*/
int squareNumber(int i, int j)
{
   // Note that (int) i/SquareSize and (int) j/SquareSize are the x-y
   // coordinates of the square that i,j is in.

   return SquareSize * ((i-1)/SquareSize) + (j-1)/SquareSize + 1;
}

/*
Overloaded output operator for vector class.
*/
ostream &operator<<(ostream &ostr, vector<int> &v)
{
   for (int i = 0; i < v.size(); i++)
      ostr << v[i] << " ";
   cout << endl;
}

/*
Returns the value stored in a cell.  Throws an exception
if bad values are passed.
*/
ValueType board::getCell(int i, int j)
{
   if (i >= 1 && i <= BoardSize && j >= 1 && j <= BoardSize)
      return value[i][j];
   else
      throw rangeError("bad value in getCell");
}

/*
Returns true if cell i,j is blank, and false otherwise.
*/
bool board::isBlank(int i, int j)
{
   if (i < 1 || i > BoardSize || j < 1 || j > BoardSize)
      throw rangeError("bad value in setCell");

   return (getCell(i,j) == Blank);
}

/*
Adds a specific cell value to a target location and updates conflict vectors
*/
void board::addCellVal(int i, int j, int val)
{
	// Do not add a value to the cell if it is blank
    if(!isBlank(i, j))
        return;

	// Set the correct coordinates in value to the input value
    value[i][j] = val;

    // Set the row, col, and square vector conflicts of the cell value to true
    row[i][val] = true;
    col[j][val] = true;
    int squareNum = squareNumber(i, j);
    square[squareNum][val] = true;
}

/*
Remove a value from a cell at a specific coordinate and update conflict vectors
*/
void board::removeCellVal(int i, int j)
{
	// Get the cell value
    int val = getCell(i, j);

    // Set the row, col, and square vector conflicts of the cell value to false
    row[i][val] = false;
    col[j][val] = false;
    int squareNum = squareNumber(i, j);
    square[squareNum][val] = false;

	// Set the coordinate in the value vector to blank
    value[i][j] = Blank;
}

/*
Prints the current board
*/
void board::print()
{
   for (int i = 1; i <= BoardSize; i++)
   {
      if ((i-1) % SquareSize == 0)
      {
         cout << " -";
	 for (int j = 1; j <= BoardSize; j++)
	    cout << "---";
         cout << "-";
	 cout << endl;
      }
      for (int j = 1; j <= BoardSize; j++)
      {
	 if ((j-1) % SquareSize == 0)
	    cout << "|";
	 if (!isBlank(i,j))
	    cout << " " << getCell(i,j) << " ";
	 else
	    cout << "   ";
      }
      cout << "|";
      cout << endl;
   }

   cout << " -";
   for (int j = 1; j <= BoardSize; j++)
      cout << "---";
   cout << "-";
   cout << endl;
}

/*
Prints the row, column, and square conflict arrays in a grid format
*/
void board::printConflicts()
{
    cout << "row conflicts:" << endl;
    cout << "    1 2 3 4 5 6 7 8 9" << endl;
    for(int i = 1; i <= BoardSize; i++)
    {
        cout << i << "   ";
        for(int s = 1; s <= BoardSize; s++)
        {
            if (row[i][s])
                cout << "T ";
            else
                cout << "F ";
        }
        cout << endl;
    }

    cout << "col conflicts:" << endl;
    cout << "    1 2 3 4 5 6 7 8 9" << endl;
    for(int i = 1; i <= BoardSize; i++)
    {
        cout << i << "   ";
        for(int s = 1; s <= BoardSize; s++)
        {
            if (col[i][s])
                cout << "T ";
            else
                cout << "F ";
        }
        cout << endl;
    }

    cout << "square conflicts:" << endl;
    cout << "    1 2 3 4 5 6 7 8 9" << endl;
    for(int i = 1; i <= BoardSize; i++)
    {
        cout << i << "   ";
        for(int s = 1; s <= BoardSize; s++)
        {
            if (square[i][s])
                cout << "T ";
            else
                cout << "F ";
        }
        cout << endl;
    }
}

/*
Return boolean whether the board is solved
*/
bool board::checkSolved()
{
    for(int i = 1; i <= BoardSize; i++)
    {
        for(int j = 1; j <= BoardSize; j++)
        {
        	// If any cell is blank, the board is not solved
            if (isBlank(i, j))
                return false;
        }
    }

    // If every cell isn't blank, the board is solved
    return true;
}
