// Alex Free, Benjamin Fox -- Project 3b
// Board class function declarations

#ifndef BOARD_H
#define BOARD_H

typedef int ValueType; // The type of the value in a cell
const int Blank = -1;  // Indicates that a cell is blank

const int SquareSize = 3;  //  The number of cells in a small square
                           //  (usually 3).  The board has
                           //  SquareSize^2 rows and SquareSize^2
                           //  columns.

const int BoardSize = SquareSize * SquareSize;

const int MinValue = 1;
const int MaxValue = 9;

//int numSolutions = 0;
int squareNumber(int i, int j);

class board
// Stores the entire Sudoku board
{
   public:
      board(int);
      void clear();
      void initialize(ifstream &fin);
      void initializeConflicts();
      void print();
      void printConflicts();
      void addCellVal(int i, int j, int val);
      void removeCellVal(int i, int j);
      bool checkSolved();
      bool isBlank(int, int);
      ValueType getCell(int, int);

   private:

      // The following matrices go from 1 to BoardSize in each
      // dimension, i.e., they are each (BoardSize+1) * (BoardSize+1)

      matrix<ValueType> value;
      matrix<bool> row;
      matrix<bool> col;
      matrix<bool> square;
};

#endif
