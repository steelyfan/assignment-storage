// Alex Free, Benjamin Fox -- Project 4a
// Main file for Project 4a


#include <iostream>
#include <limits.h>
#include "d_matrix.h"
#include "d_except.h"
#include "board.h"
#include <list>
#include <fstream>

using namespace std;

/*
Main part of program
*/
int main()
{
   	ifstream fin;

   	// Read the sample grid from the file.
   	string fileName = "sudoku1.txt";
   	fin.open(fileName.c_str());
   	if (!fin)
   	{
      	cerr << "Cannot open " << fileName << endl;
      	exit(1);
   	}

   try
   {
   		// Initialize the board
      	board b1(SquareSize);

		// Read every line in the text file
      	while (fin && fin.peek() != 'Z')
      	{
      		// Initialize the board, print it, initialize conflicts, print conflicts, and check whether board is solved
	      	b1.initialize(fin);
	      	b1.print();
	      	b1.initializeConflicts();
	      	b1.printConflicts();
	      	if (b1.checkSolved())
	         	cout << "Board solved";
	      	else
	         	cout << "Board not solved.";
       	}
   }
   catch  (indexRangeError &ex)
   {
      	cout << ex.what() << endl;
      	exit(1);
   }
}
