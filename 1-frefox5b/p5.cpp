// Alex Free, Benjamin Fox -- Project 5b
// Maze class and main function

#include <iostream>
#include <limits.h>
#include "d_except.h"
#include <list>
#include <fstream>
#include "d_matrix.h"
#include "graph.h"
#include <stack>
#include <queue>
#include <cmath>


using namespace std;

// Declaration of maze class
class maze
{
   public:
      maze(ifstream &fin);
      void print(int,int,int,int);
      bool isLegal(int i, int j);

      void findPathRecursive(graph &g, int x, int y);
      void findPathNonRecursive(graph &g);
      bool findShortestPath1 (graph &g);
      bool findShortestPath2 (graph &g);
      vector<int> positionFromId(int id);

      void printSolution(graph &g);

      void setMap(int i, int j, int n);
      int getMap(int i, int j) const;
      void mapMazeToGraph(graph &g);

   private:
      int rows; // number of rows in the maze
      int cols; // number of columns in the maze
      bool pathFound = false; // keeps track of whether path found so only 1 solution found

      matrix<bool> value;
      matrix<int> map;      // Mapping from maze (i,j) values to node index values
};

maze::maze(ifstream &fin)
// Initializes a maze by reading values from fin.  Assumes that the
// number of rows and columns indicated in the file are correct.
{
   fin >> rows;
   fin >> cols;

   char x;

   value.resize(rows,cols);
   for (int i = 0; i <= rows-1; i++)
      for (int j = 0; j <= cols-1; j++)
      {
	 fin >> x;
	 if (x == 'O')
            value[i][j] = true;
	 else
	    value[i][j] = false;
      }

   map.resize(rows,cols);
}

void maze::print(int goalI, int goalJ, int currI, int currJ)
// Print out a maze, with the goal and current cells marked on the
// board.
{
   cout << endl;

   if (goalI < 0 || goalI > rows || goalJ < 0 || goalJ > cols)
      throw rangeError("Bad value in maze::print");

   if (currI < 0 || currI > rows || currJ < 0 || currJ > cols)
      throw rangeError("Bad value in maze::print");

   for (int i = 0; i <= rows-1; i++)
   {
      for (int j = 0; j <= cols-1; j++)
      {
	 if (i == goalI && j == goalJ)
	    cout << "*";
	 else
	    if (i == currI && j == currJ)
	       cout << "+";
	    else
	       if (value[i][j])
		  cout << " ";
	       else
		  cout << "X";
      }
      cout << endl;
   }
   cout << endl;
}

bool maze::isLegal(int i, int j)
// Return the value stored at the (i,j) entry in the maze.
{
   if (i < 0 || i > rows || j < 0 || j > cols)
      throw rangeError("Bad value in maze::isLegal");

   return value[i][j];
}

void maze::mapMazeToGraph(graph &g)
// Create a graph g that represents the legal moves in the maze m.
{

	// Add nodes where there are legal cells and put ID in correct spot in map
	for(int i = 0; i < value.rows(); i++)
	{
		for(int j = 0; j < value.cols(); j++)
		{
			if(isLegal(i, j))
			{
				map[i][j] = g.addNode();

			}
		}
	}

	// Loop through every cell in maze
	for(int i = 0; i < value.rows(); i++)
	{
		for(int j = 0; j < value.cols(); j++)
		{
			// Only continue if cell is legal
			if(isLegal(i, j))
			{
				// Look at 4 bordering cells (only if they exist in maze)
				// If the bordering cell is a node, add edge between current cell and bordering cell
				if(i - 1 >= 0 && isLegal(i-1, j))
					g.addEdge(map[i][j], map[i -1][j], 1);

				if(i + 1 < value.rows() && isLegal(i+1, j))
					g.addEdge(map[i][j], map[i + 1] [j], 1);

				if(j - 1 >= 0 && isLegal(i, j-1))
					g.addEdge(map[i][j], map[i][j - 1], 1);

				if(j + 1 < value.cols() && isLegal(i, j+1))
					g.addEdge(map[i][j], map[i][j + 1], 1);
			}
		}
	}
}

/**
Find a path through the maze using a recursive algorithm
Finds path in graph g to end from starting node at (row, cols)
**/
void maze::findPathRecursive(graph &g, int row, int cols)
{
	// Mark the current node as part of the final path
	// Visit current node
	g.mark(map[row][cols]);
	g.visit(map[row][cols]);

	// If the current coordinates are bottom right,
	//		Flag that path was found and return (don't continue finding path)
	if(row == value.rows() - 1 && cols == value.cols() - 1)
	{
		pathFound = true;
		return;
	}

	// If a path was found previously, don't keep looking
	if(pathFound)
	{
		return;
	}

	// Look at 4 potential bordering nodes
	// If bordering node has coordinates in graph, has an edge with current node, and isn't visited already,
	//		Find a path from the bordering node
	if(row + 1 < value.rows() && isLegal(row+1, cols)&& g.isEdge(map[row][cols], map[row+1][cols]) && !g.isVisited(map[row+1][cols]))
		findPathRecursive(g, row+1, cols);

	if(cols + 1 < value.cols() && isLegal(row, cols+1)&& g.isEdge(map[row][cols], map[row][cols+1]) && !g.isVisited(map[row][cols+1]))
		findPathRecursive(g, row, cols+1);

	if(row - 1 >= 0 && isLegal(row-1, cols)&& g.isEdge(map[row][cols], map[row-1][cols]) && !g.isVisited(map[row-1][cols]))
		findPathRecursive(g, row-1, cols);

	if(cols - 1 >= 0 && isLegal(row, cols-1)&& g.isEdge(map[row][cols], map[row][cols-1]) && !g.isVisited(map[row][cols-1]))
		findPathRecursive(g, row, cols-1);

	// If no path found from current node,
	//		Unmark current node from final path
	if(!pathFound)
		g.unMark(map[row][cols]);
}

/*
*findPathNonRecursive takes in a graph g and uses a non-recursive implementation
*of DFS to traverse the maze and find a solution.
*/
void maze::findPathNonRecursive(graph &g)
{
	g.clearMark(); //clear previous marked/visited nodes
	g.clearVisit();
	g.visit(map[0][0]); //visit and mark the starting node
	g.mark(map[0][0]);

	pathFound = false;

	stack<vector<int> > positions; //initialize stack of coordinates
	vector<int> v = {0, 0};
	positions.push(v); //add starting coordinates
	vector<int> w = v;

	while(!positions.empty() && !pathFound) //loop while stack isn't empty and path isn't found
	{
		v = positions.top(); //set v equal to top of stack


		if(v.at(0) == value.rows() - 1 && v.at(1) == value.cols() - 1) //if the goal is reached
		{
			pathFound = true;
			g.visit(map[v.at(0)][v.at(1)]); //visit and mark current position
			g.mark(map[v.at(0)][v.at(1)]);
		}

		//elseif statement for each direction (down, right, up, left)
		else if(v.at(0)+1 < value.rows() && isLegal(v.at(0)+1, v.at(1))&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)+1][v.at(1)]) && !g.isVisited(map[v.at(0)+1][v.at(1)])) //if move is legal and node is unvisited
		{
			g.visit(map[v.at(0)+1][v.at(1)]); //visit and mark incremented position
			g.mark(map[v.at(0)+1][v.at(1)]);
			w.at(0) = v.at(0)+1; //set w equal to incremented v and push w to stack
			w.at(1) = v.at(1);
			positions.push(w);
		}
		else if(v.at(1)+1 < value.cols() && isLegal(v.at(0), v.at(1)+1)&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)][v.at(1)+1]) && !g.isVisited(map[v.at(0)][v.at(1)+1]))
		{
			g.visit(map[v.at(0)][v.at(1)+1]);
			g.mark(map[v.at(0)][v.at(1)+1]);
			w.at(0) = v.at(0);
			w.at(1) = v.at(1)+1;
			positions.push(w);
		}
		else if(v.at(0)-1 >= 0 && isLegal(v.at(0)-1, v.at(1))&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)-1][v.at(1)]) && !g.isVisited(map[v.at(0)-1][v.at(1)]))
		{
			g.visit(map[v.at(0)-1][v.at(1)]);
			g.mark(map[v.at(0)-1][v.at(1)]);
			w.at(0) = v.at(0)-1;
			w.at(1) = v.at(1);
			positions.push(w);
		}
		else if(v.at(1)-1 >= 0 && isLegal(v.at(0), v.at(1)+1)&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)][v.at(1)+1]) && !g.isVisited(map[v.at(0)][v.at(1)+1]))
		{
			g.visit(map[v.at(0)][v.at(1)-1]);
			g.mark(map[v.at(0)][v.at(1)-1]);
			w.at(0) = v.at(0);
			w.at(1) = v.at(1)-1;
			positions.push(w);
		}
		else //if no legal/unvisited moves remain
		{
			positions.pop(); //pop from stack and unmark
			g.unMark(map[v.at(0)][v.at(1)]);
		}
	}
}

/*
* printSolution prints the moves of a solution to a maze. It takes in a graph
*representation of a solved maze
*/
void maze::printSolution(graph &g)
{
	int row = 0; //initialize row (y-axis coordinate) and cols (x-axis coordinate)
	int cols = 0;

	print(value.rows()-1, value.cols()-1, row, cols); //call print function for starting position

	while(row < value.rows()-1 || cols < value.cols()-1) //loop while within boundaries
	{
		//four if-statements cover four directions (down, right, up, left)
		if(row + 1 < value.rows() && isLegal(row+1, cols) && g.isEdge(map[row][cols], map[row+1][cols]) && g.isMarked(map[row+1][cols]))
		{ //if move is legal and the node is marked
			g.unMark(map[row][cols]);
			row++; //increment row position (make the move)
			cout << "Go down" << endl;
			print(value.rows()-1, value.cols()-1, row, cols); //print the new state
		}
		else if(cols + 1 < value.cols() && isLegal(row, cols+1) && g.isEdge(map[row][cols], map[row][cols+1]) && g.isMarked(map[row][cols+1]))
		{
			g.unMark(map[row][cols]);
			cols++;
			cout << "Go right" << endl;
			print(value.rows()-1, value.cols()-1, row, cols);
		}
		else if(row - 1 >= 0 && isLegal(row-1, cols) && g.isEdge(map[row][cols], map[row-1][cols]) && g.isMarked(map[row-1][cols]))
		{
			g.unMark(map[row][cols]);
			row--;
			cout << "Go up" << endl;
			print(value.rows()-1, value.cols()-1, row, cols);
		}
		else if(cols - 1 >= 0 && isLegal(row, cols-1) && g.isEdge(map[row][cols], map[row][cols-1]) && g.isMarked(map[row][cols-1]))
		{
			g.unMark(map[row][cols]);
			cols--;
			cout << "Go left" << endl;
			print(value.rows()-1, value.cols()-1, row, cols);
		}
		else
		{
			cout << "No solution!" << endl; //if none of the above four conditions are met, print this and exit the function
			return;
		}
	}
}

/**
Using breadth-first search
Find the shortest path from the top left to the bottom right of the maze with graph g
**/
bool maze::findShortestPath1(graph &g)
{
	// Clear all nodes from being marked and visited
	g.clearMark();
	g.clearVisit();

	// Visit and mark the top left node
	g.visit(map[0][0]);
	g.mark(map[0][0]);

	// Create a queue of positions for breadth-first search
	queue<vector<int> > q;

	// Push the top left position into the queue
	vector<int> v = {0, 0};
	q.push(v);
	vector<int> w = v;

	// Keep a vector of each node's predecessor position and initialize all with (-1, -1)
	vector<vector<int >> predecessors(value.rows()*value.cols(), {-1, -1});

	// Keep track of whether a path has been found
	pathFound = false;

	// Keep trying to find path while the queue is not empty
	while (!q.empty())
	{
		// Set current node position to front of queue
		v = q.front();

		// If the current node position is the bottom right, a path has been found and break out of while loop
		if (v.at(0) == value.rows() - 1 && v.at(1) == value.cols() - 1)
		{
			pathFound = true;
			break;
		}

		// Following repeated for neighbors that are below, right of, above, and left of current node (in order)
		// If neighbor is within grid limits, is a node, has an edge with the current node, and hasn't been visited
		//		Visit the neighbor
		//		Push the neighbor into the queue
		//		Set the neighbor's predecessor position to the current node position
		if(v.at(0)+1 < value.rows() && isLegal(v.at(0)+1, v.at(1))&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)+1][v.at(1)]) && !g.isVisited(map[v.at(0)+1][v.at(1)])) //if move is legal and node is unvisited
		{
			g.visit(map[v.at(0)+1][v.at(1)]); //visit and mark incremented position
			w.at(0) = v.at(0)+1; //set w equal to incremented v and push w to stack
			w.at(1) = v.at(1);
			q.push(w);
			predecessors.at(map[w.at(0)][w.at(1)]) = v;
		}

		// Right neighbor
		if(v.at(1)+1 < value.cols() && isLegal(v.at(0), v.at(1)+1)&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)][v.at(1)+1]) && !g.isVisited(map[v.at(0)][v.at(1)+1]))
		{
			g.visit(map[v.at(0)][v.at(1)+1]);
			w.at(0) = v.at(0);
			w.at(1) = v.at(1)+1;
			q.push(w);
			predecessors.at(map[w.at(0)][w.at(1)]) = v;
		}

		// Above neighbor
		if(v.at(0)-1 >= 0 && isLegal(v.at(0)-1, v.at(1))&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)-1][v.at(1)]) && !g.isVisited(map[v.at(0)-1][v.at(1)]))
		{
			g.visit(map[v.at(0)-1][v.at(1)]);
			w.at(0) = v.at(0)-1;
			w.at(1) = v.at(1);
			q.push(w);
			predecessors.at(map[w.at(0)][w.at(1)]) = v;
		}

		// Left neigbor
		if(v.at(1)-1 >= 0 && isLegal(v.at(0), v.at(1)+1)&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)][v.at(1)+1]) && !g.isVisited(map[v.at(0)][v.at(1)+1]))
		{
			g.visit(map[v.at(0)][v.at(1)-1]);
			w.at(0) = v.at(0);
			w.at(1) = v.at(1)-1;
			q.push(w);
			predecessors.at(map[w.at(0)][w.at(1)]) = v;
		}

		// Pop the current node position off the queue
		q.pop();
	}

	// Begin at bottom right node
	int row = value.rows()-1;
	int col = value.cols()-1;

	// While the predecessor position for the node is not (-1, -1) (meaning node has a predecessor)
	while(predecessors.at(map[row][col]).at(0) != -1 && predecessors.at(map[row][col]).at(1) != -1)
	{
		// Mark the current node
		g.mark(map[row][col]);

		// Set next row and column to predecessor row and column of the current node
		int temp_row = row;
		row = predecessors.at(map[row][col]).at(0);
		col = predecessors.at(map[temp_row][col]).at(1);
	}

	// Return whether a path was found
	return pathFound;

}

/**
Get the position of a node as a vector from its ID
**/
vector<int> maze::positionFromId(int id)
{
	// Loop through every element of map
	for(int i = 0; i < map.rows(); i++)
	{
		for(int j = 0; j < map.cols(); j++)
		{
			// If the current map value is the right ID, return the position
			if(map[i][j] == id)
				return {i, j};
		}
	}

	// If reached, ID was not found
	// Return position of (-1, -1) in that case
	return {-1, -1};
}


/*
*findShortestPath 2 uses Djikstra's algorithm to find the shortest path in a given maze represented by a graph
*it takes in the graph/maze, and returns a boolean
*/
bool maze::findShortestPath2(graph &g)
{
	pathFound = false;
	g.clearMark(); //clear previous marked/visited nodes
	g.clearVisit();
	g.mark(map[0][0]); //mark the starting node
	g.getNode(0).setWeight(0); //set it's weight to zero
	priority_queue<node, vector<node>, greater<node>> pq; //declare priority queue
	pq.push(g.getNode(0));  //push starting node to priority queue
	vector<int> w = {0, 0};
	for(int i = 1; i < g.numNodes(); i++) // add the rest of the nodes to the queue, setting their weights to a high number
	{
		g.getNode(i).setWeight(1000000);
		pq.push(g.getNode(i));

	}



	while(!pq.empty() && !pathFound) //while the queue is not empty and the path is not found
	{
		g.visit(pq.top().getId()); //visit the top of the queue
		vector<int> v = positionFromId(pq.top().getId()); //declare v to be the top of the queue
		pq.pop(); //pop the top of the priority queue

		if(map[v.at(0)][v.at(1)] == g.numNodes()-1) //if v is the goal node
		{
			pathFound = true; //the path has been found
		}
		else
		{
			//four if-statements for four directions (down, right, up, left)
			if(v.at(0)+1 < value.rows() && isLegal(v.at(0)+1, v.at(1))&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)+1][v.at(1)]) && !g.isVisited(map[v.at(0)+1][v.at(1)])) //if move is legal and node is unvisited
			{

				w.at(0) = v.at(0)+1; // make the move (set w to incremented v)
				w.at(1) = v.at(1);
				int SPold1 = g.getNode(map[w.at(0)][w.at(1)]).getWeight(); //weight of w
				int SPold2 = g.getNode(map[v.at(0)][v.at(1)]).getWeight() + 1; //weight of v + 1 (distance between w and v)

				if (SPold2 < SPold1) //if weight of v -> w is less than weight of w
				{
					g.getNode(map[w.at(0)][w.at(1)]).setParent(map[v.at(0)][v.at(1)]); //set parent of w to v
					g.getNode(map[w.at(0)][w.at(1)]).setWeight(SPold2); //set weight of w to v -> w
					pq.push(g.getNode(map[w.at(0)][w.at(1)])); //push this altered node into the priority queue
				}
			}


			if(v.at(1)+1 < value.cols() && isLegal(v.at(0), v.at(1)+1)&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)][v.at(1)+1]) && !g.isVisited(map[v.at(0)][v.at(1)+1]))
			{

				w.at(0) = v.at(0);
				w.at(1) = v.at(1)+1;
				int SPold1 = g.getNode(map[w.at(0)][w.at(1)]).getWeight();
				int SPold2 = g.getNode(map[v.at(0)][v.at(1)]).getWeight() + 1;

				if (SPold2 < SPold1)
				{
					g.getNode(map[w.at(0)][w.at(1)]).setParent(map[v.at(0)][v.at(1)]);
					g.getNode(map[w.at(0)][w.at(1)]).setWeight(SPold2);
					pq.push(g.getNode(map[w.at(0)][w.at(1)]));
				}

			}

			if(v.at(0)-1 >= 0 && isLegal(v.at(0)-1, v.at(1))&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)-1][v.at(1)]) && !g.isVisited(map[v.at(0)-1][v.at(1)]))
			{
				w.at(0) = v.at(0)-1;
				w.at(1) = v.at(1);
				int SPold1 = g.getNode(map[w.at(0)][w.at(1)]).getWeight();
				int SPold2 = g.getNode(map[v.at(0)][v.at(1)]).getWeight() + 1;

				if (SPold2 < SPold1)
				{
					g.getNode(map[w.at(0)][w.at(1)]).setParent(map[v.at(0)][v.at(1)]);
					g.getNode(map[w.at(0)][w.at(1)]).setWeight(SPold2);
					pq.push(g.getNode(map[w.at(0)][w.at(1)]));
				}
			}

			if(v.at(1)-1 >= 0 && isLegal(v.at(0), v.at(1)+1)&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)][v.at(1)+1]) && !g.isVisited(map[v.at(0)][v.at(1)+1]))
			{
				w.at(0) = v.at(0);
				w.at(1) = v.at(1)-1;
				int SPold1 = g.getNode(map[w.at(0)][w.at(1)]).getWeight();
				int SPold2 = g.getNode(map[v.at(0)][v.at(1)]).getWeight() + 1;

				if (SPold2 < SPold1)
				{
					g.getNode(map[w.at(0)][w.at(1)]).setParent(map[v.at(0)][v.at(1)]);
					g.getNode(map[w.at(0)][w.at(1)]).setWeight(SPold2);
					pq.push(g.getNode(map[w.at(0)][w.at(1)]));
				}
			}
		}

	}

	int curr = g.getNode(map[value.rows()-1][value.cols()-1]).getId(); //curr = last node
	while(curr != 0) //
	{
		g.mark(curr); //mark curr
		curr = g.getNode(curr).getParent(); //assign curr's parent to curr
	}

	return pathFound; //return whether or not a path was found
}


int main()
{
   char x;
   ifstream fin;

   // Read the maze from the file.
   string fileName = "maze1.txt";

   fin.open(fileName.c_str());
   if (!fin)
   {
      cerr << "Cannot open " << fileName << endl;
      exit(1);
   }

   try
   {
		// Create graph g
      	graph g;

      	// Look through all mazes in file
      	while (fin && fin.peek() != 'Z')
      	{
      		// Initialize maze m
         	maze m(fin);

         	// Create graph from maze object
         	m.mapMazeToGraph(g);

			// Find BFS shortest path and print solution
			cout << "BFS: " << endl;
 		 	m.findShortestPath1(g);
 		 	m.printSolution(g);

			cout << endl << endl;

			// Find Dijkstra shortest path and print solution
 		 	cout << "Dijkstra: " << endl;
 		 	m.findShortestPath2(g);
 		 	m.printSolution(g);
      }

   }
   catch (indexRangeError &ex)
   {
      cout << ex.what() << endl; exit(1);
   }
   catch (rangeError &ex)
   {
      cout << ex.what() << endl; exit(1);
   }
}
