// Alex Free, Benjamin Fox -- Project 51
// Maze class and main function

#include <iostream>
#include <limits.h>
#include "d_except.h"
#include <list>
#include <fstream>
#include "d_matrix.h"
#include "graph.h"
#include <stack>

using namespace std;

// Declaration of maze class
class maze
{
   public:
      maze(ifstream &fin);
      void print(int,int,int,int);
      bool isLegal(int i, int j);

      void findPathRecursive(graph &g, int x, int y);
      void findPathNonRecursive(graph &g);

      void printSolution(graph &g);

      void setMap(int i, int j, int n);
      int getMap(int i, int j) const;
      void mapMazeToGraph(graph &g);

   private:
      int rows; // number of rows in the maze
      int cols; // number of columns in the maze
      bool pathFound = false; // keeps track of whether path found so only 1 solution found

      matrix<bool> value;
      matrix<int> map;      // Mapping from maze (i,j) values to node index values
};

maze::maze(ifstream &fin)
// Initializes a maze by reading values from fin.  Assumes that the
// number of rows and columns indicated in the file are correct.
{
   fin >> rows;
   fin >> cols;

   char x;

   value.resize(rows,cols);
   for (int i = 0; i <= rows-1; i++)
      for (int j = 0; j <= cols-1; j++)
      {
	 fin >> x;
	 if (x == 'O')
            value[i][j] = true;
	 else
	    value[i][j] = false;
      }

   map.resize(rows,cols);
}

void maze::print(int goalI, int goalJ, int currI, int currJ)
// Print out a maze, with the goal and current cells marked on the
// board.
{
   cout << endl;

   if (goalI < 0 || goalI > rows || goalJ < 0 || goalJ > cols)
      throw rangeError("Bad value in maze::print");

   if (currI < 0 || currI > rows || currJ < 0 || currJ > cols)
      throw rangeError("Bad value in maze::print");

   for (int i = 0; i <= rows-1; i++)
   {
      for (int j = 0; j <= cols-1; j++)
      {
	 if (i == goalI && j == goalJ)
	    cout << "*";
	 else
	    if (i == currI && j == currJ)
	       cout << "+";
	    else
	       if (value[i][j])
		  cout << " ";
	       else
		  cout << "X";
      }
      cout << endl;
   }
   cout << endl;
}

bool maze::isLegal(int i, int j)
// Return the value stored at the (i,j) entry in the maze.
{
   if (i < 0 || i > rows || j < 0 || j > cols)
      throw rangeError("Bad value in maze::isLegal");

   return value[i][j];
}

void maze::mapMazeToGraph(graph &g)
// Create a graph g that represents the legal moves in the maze m.
{

	// Add nodes where there are legal cells and put ID in correct spot in map
	for(int i = 0; i < value.rows(); i++)
	{
		for(int j = 0; j < value.cols(); j++)
		{
			if(isLegal(i, j))
			{
				map[i][j] = g.addNode();

			}
		}
	}

	// Loop through every cell in maze
	for(int i = 0; i < value.rows(); i++)
	{
		for(int j = 0; j < value.cols(); j++)
		{
			// Only continue if cell is legal
			if(isLegal(i, j))
			{
				// Look at 4 bordering cells (only if they exist in maze)
				// If the bordering cell is a node, add edge between current cell and bordering cell
				if(i - 1 >= 0 && isLegal(i-1, j))
					g.addEdge(map[i][j], map[i -1][j], 1);

				if(i + 1 < value.rows() && isLegal(i+1, j))
					g.addEdge(map[i][j], map[i + 1] [j], 1);

				if(j - 1 >= 0 && isLegal(i, j-1))
					g.addEdge(map[i][j], map[i][j - 1], 1);

				if(j + 1 < value.cols() && isLegal(i, j+1))
					g.addEdge(map[i][j], map[i][j + 1], 1);
			}
		}
	}
}

/**
Find a path through the maze using a recursive algorithm
Finds path in graph g to end from starting node at (row, cols)
**/
void maze::findPathRecursive(graph &g, int row, int cols)
{
	// Mark the current node as part of the final path
	// Visit current node
	g.mark(map[row][cols]);
	g.visit(map[row][cols]);

	// If the current coordinates are bottom right,
	//		Flag that path was found and return (don't continue finding path)
	if(row == value.rows() - 1 && cols == value.cols() - 1)
	{
		pathFound = true;
		return;
	}

	// If a path was found previously, don't keep looking
	if(pathFound)
	{
		return;
	}

	// Look at 4 potential bordering nodes
	// If bordering node has coordinates in graph, has an edge with current node, and isn't visited already,
	//		Find a path from the bordering node
	if(row + 1 < value.rows() && isLegal(row+1, cols)&& g.isEdge(map[row][cols], map[row+1][cols]) && !g.isVisited(map[row+1][cols]))
		findPathRecursive(g, row+1, cols);

	if(cols + 1 < value.cols() && isLegal(row, cols+1)&& g.isEdge(map[row][cols], map[row][cols+1]) && !g.isVisited(map[row][cols+1]))
		findPathRecursive(g, row, cols+1);

	if(row - 1 >= 0 && isLegal(row-1, cols)&& g.isEdge(map[row][cols], map[row-1][cols]) && !g.isVisited(map[row-1][cols]))
		findPathRecursive(g, row-1, cols);

	if(cols - 1 >= 0 && isLegal(row, cols-1)&& g.isEdge(map[row][cols], map[row][cols-1]) && !g.isVisited(map[row][cols-1]))
		findPathRecursive(g, row, cols-1);

	// If no path found from current node,
	//		Unmark current node from final path
	if(!pathFound)
		g.unMark(map[row][cols]);
}

/*
*findPathNonRecursive takes in a graph g and uses a non-recursive implementation
*of DFS to traverse the maze and find a solution.
*/
void maze::findPathNonRecursive(graph &g)
{
	g.clearMark(); //clear previous marked/visited nodes
	g.clearVisit();
	g.visit(map[0][0]); //visit and mark the starting node
	g.mark(map[0][0]);

	pathFound = false;

	stack<vector<int> > positions; //initialize stack of coordinates
	vector<int> v = {0, 0};
	positions.push(v); //add starting coordinates
	vector<int> w = v;

	while(!positions.empty() && !pathFound) //loop while stack isn't empty and path isn't found
	{
		v = positions.top(); //set v equal to top of stack


		if(v.at(0) == value.rows() - 1 && v.at(1) == value.cols() - 1) //if the goal is reached
		{
			pathFound = true;
			g.visit(map[v.at(0)][v.at(1)]); //visit and mark current position
			g.mark(map[v.at(0)][v.at(1)]);
		}

		//elseif statement for each direction (down, right, up, left)
		else if(v.at(0)+1 < value.rows() && isLegal(v.at(0)+1, v.at(1))&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)+1][v.at(1)]) && !g.isVisited(map[v.at(0)+1][v.at(1)])) //if move is legal and node is unvisited
		{
			g.visit(map[v.at(0)+1][v.at(1)]); //visit and mark incremented position
			g.mark(map[v.at(0)+1][v.at(1)]);
			w.at(0) = v.at(0)+1; //set w equal to incremented v and push w to stack
			w.at(1) = v.at(1);
			positions.push(w);
		}
		else if(v.at(1)+1 < value.cols() && isLegal(v.at(0), v.at(1)+1)&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)][v.at(1)+1]) && !g.isVisited(map[v.at(0)][v.at(1)+1]))
		{
			g.visit(map[v.at(0)][v.at(1)+1]);
			g.mark(map[v.at(0)][v.at(1)+1]);
			w.at(0) = v.at(0);
			w.at(1) = v.at(1)+1;
			positions.push(w);
		}
		else if(v.at(0)-1 >= 0 && isLegal(v.at(0)-1, v.at(1))&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)-1][v.at(1)]) && !g.isVisited(map[v.at(0)-1][v.at(1)]))
		{
			g.visit(map[v.at(0)-1][v.at(1)]);
			g.mark(map[v.at(0)-1][v.at(1)]);
			w.at(0) = v.at(0)-1;
			w.at(1) = v.at(1);
			positions.push(w);
		}
		else if(v.at(1)-1 >= 0 && isLegal(v.at(0), v.at(1)+1)&& g.isEdge(map[v.at(0)][v.at(1)], map[v.at(0)][v.at(1)+1]) && !g.isVisited(map[v.at(0)][v.at(1)+1]))
		{
			g.visit(map[v.at(0)][v.at(1)-1]);
			g.mark(map[v.at(0)][v.at(1)-1]);
			w.at(0) = v.at(0);
			w.at(1) = v.at(1)-1;
			positions.push(w);
		}
		else //if no legal/unvisited moves remain
		{
			positions.pop(); //pop from stack and unmark
			g.unMark(map[v.at(0)][v.at(1)]);
		}
	}
}

/*
* printSolution prints the moves of a solution to a maze. It takes in a graph
*representation of a solved maze
*/
void maze::printSolution(graph &g)
{
	int row = 0; //initialize row (y-axis coordinate) and cols (x-axis coordinate)
	int cols = 0;

	print(value.rows()-1, value.cols()-1, row, cols); //call print function for starting position

	while(row < value.rows()-1 || cols < value.cols()-1) //loop while within boundaries
	{
		//four if-statements cover four directions (down, right, up, left)
		if(row + 1 < value.rows() && isLegal(row+1, cols) && g.isEdge(map[row][cols], map[row+1][cols]) && g.isMarked(map[row+1][cols]))
		{ //if move is legal and the node is marked
			row++; //increment row position (make the move)
			cout << "Go down" << endl;
			print(value.rows()-1, value.cols()-1, row, cols); //print the new state
		}
		else if(cols + 1 < value.cols() && isLegal(row, cols+1) && g.isEdge(map[row][cols], map[row][cols+1]) && g.isMarked(map[row][cols+1]))
		{
			cols++;
			cout << "Go right" << endl;
			print(value.rows()-1, value.cols()-1, row, cols);
		}
		else if(row - 1 >= 0 && isLegal(row-1, cols) && g.isEdge(map[row][cols], map[row-1][cols]) && g.isMarked(map[row-1][cols]))
		{
			row--;
			cout << "Go up" << endl;
			print(value.rows()-1, value.cols()-1, row, cols);
		}
		else if(cols - 1 >= 0 && isLegal(row, cols-1) && g.isEdge(map[row][cols], map[row][cols-1]) && g.isMarked(map[row][cols-1]))
		{
			cols--;
			cout << "Go left" << endl;
			print(value.rows()-1, value.cols()-1, row, cols);
		}
		else
		{
			cout << "No solution!" << endl; //if none of the above four conditions are met, print this and exit the function
			return;
		}
	}
}

int main()
{
   char x;
   ifstream fin;

   // Read the maze from the file.
   string fileName = "maze1.txt";

   fin.open(fileName.c_str());
   if (!fin)
   {
      cerr << "Cannot open " << fileName << endl;
      exit(1);
   }

   try
   {
		// Create graph g
      	graph g;

      	// Look through all mazes in file
      	while (fin && fin.peek() != 'Z')
      	{
      		// Initialize maze m
         	maze m(fin);

         	// Create graph from maze object
         	m.mapMazeToGraph(g);

			// Find path to end recursively and print solution
         	cout << "Recursive solution!" << endl << endl;
         	m.findPathRecursive(g, 0, 0);
         	m.printSolution(g);

			// Find path to end non-recursively and print solution
         	cout << "Nonrecursive solution!" << endl << endl;
         	m.findPathNonRecursive(g);
 		 	m.printSolution(g);
      }

   }
   catch (indexRangeError &ex)
   {
      cout << ex.what() << endl; exit(1);
   }
   catch (rangeError &ex)
   {
      cout << ex.what() << endl; exit(1);
   }
}
