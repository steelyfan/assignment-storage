// Alex Free, Benjamin Fox -- Project 4b
// Main file for Project 4b


#include <iostream>
#include <limits.h>
#include "d_matrix.h"
#include "d_except.h"
#include "board.h"
#include <list>
#include <fstream>

using namespace std;

/*
Main part of program
*/
int main()
{
   	ifstream fin;

   	// Read the sample grid from the file.
   	string fileName = "sudoku.txt";
   	fin.open(fileName.c_str());
   	if (!fin)
   	{
      	cerr << "Cannot open " << fileName << endl;
      	exit(1);
   	}

   try
   {
   		// Initialize the board
      	board b1(SquareSize);

		// Read every line in the text file
      	while (fin && fin.peek() != 'Z')
      	{
      		// Reset number of solutions and current recursive calls every board
      		currentCalls = 0;
      		numSolutions = 0;
      		
      		// Initialize the board, initialize conflicts, and solve
	      	b1.initialize(fin);
	      	b1.initializeConflicts();
	      	b1.solve();
	      	
	      	// Add current recursive calls to total recursive calls
	        totalCalls += currentCalls;
	        boardNumber++; // Increase number of boards looped through
	        // Print out number of recursive calls for current board
	      	cout << "Calls for board " << boardNumber << ": " << currentCalls << endl;
       	}
   }
   catch  (indexRangeError &ex)
   {
      	cout << ex.what() << endl;
      	exit(1);
   }
   
   // Print out average recursive calls and total recursive calls
   cout << "Avg number of calls: " << (float)totalCalls/(float)boardNumber << endl;
   cout << "Total number of calls: " << totalCalls << endl;

}
