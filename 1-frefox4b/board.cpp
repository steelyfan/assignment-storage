// Alex Free, Benjamin Fox -- Project 4b
// Board class implementation

#include <iostream>
#include <limits.h>
#include "d_matrix.h"
#include "d_except.h"
#include "board.h"
#include <list>
#include <fstream>

using namespace std;

// Keep track of recursive calls
int currentCalls = 0;
int totalCalls = 0;
int boardNumber = 0;

// Keep track of number of solutions
int numSolutions = 0;

/*
Board constructor initializes value, row, col, and square matrices
Clears board
*/
board::board(int sqSize)
   : value(BoardSize+1,BoardSize+1),
     row(BoardSize+1,BoardSize+1, false),
     col(BoardSize+1,BoardSize+1, false),
     square (BoardSize+1,BoardSize+1, false)
{

   clear();
}

/*
Mark all cells as blank to clear the board
*/
void board::clear()
{
   for (int i = 1; i <= BoardSize; i++)
      for (int j = 1; j <= BoardSize; j++)
      {
         value[i][j] = Blank;
      }
}

/*
Initialize the board with values from a file stream
*/
void board::initialize(ifstream &fin)
{
   char ch;

   clear(); // Clear board

   for (int i = 1; i <= BoardSize; i++)
      for (int j = 1; j <= BoardSize; j++)
	    {
	       fin >> ch; // Read a character by row then column

          // If the read char is not Blank
	      if (ch != '.')
             addCellVal(i,j,ch-'0');   // Convert char to int
        }
}

/*
Initialize the row, col, and square conflict arrays for a starting board
*/
void board::initializeConflicts()
{

	// Reset conflicts to false before initializing
	for (int i = 1; i <= BoardSize; i++)
	{
		for (int j = 1; j <= BoardSize; j++)
		{
			row[i][j] = false;
			col[i][j] = false;
			square[i][j] = false;
		}
	}


	// Loop by every row and column
   	for (int i = 1; i <= BoardSize; i++)
   	{
      for (int j = 1; j <= BoardSize; j++)
      {
      		// Only place conflicts if the current cell is not blank
            if(!isBlank(i, j))
            {
            	// Retreive the cell value
                int cellVal = getCell(i, j);

                // Set the row and column conflicts of the cell value to true
                row[i][cellVal] =  true;
                col[j][cellVal] = true;

                // Set the correct square conflict of the cell value to true
                int squareNum = squareNumber(i, j);
                square[squareNum][cellVal] = true;
            }
        }
    }
}

/*
Return the square number of cell i,j (counting from left to right,
top to bottom.  Note that i and j each go from 1 to BoardSize
*/
int squareNumber(int i, int j)
{
   // Note that (int) i/SquareSize and (int) j/SquareSize are the x-y
   // coordinates of the square that i,j is in.

   return SquareSize * ((i-1)/SquareSize) + (j-1)/SquareSize + 1;
}

/*
Calculates the row number in the board given a square number and index within the square (1-9)
*/
int rowOfSquare(int square, int numInSquare)
{
	return SquareSize * ((square - 1) / 3) + ((numInSquare - 1) / 3) + 1;
}

/*
Calculates the column number in the board given a square number and index within the square (1-9)
*/
int colOfSquare(int square, int numInSquare)
{
	return SquareSize * ((square - 1) % 3) + ((numInSquare - 1) % 3) + 1;
}

/*
Overloaded output operator for vector class.
*/
ostream &operator<<(ostream &ostr, vector<int> &v)
{
   for (int i = 0; i < v.size(); i++)
      ostr << v[i] << " ";
   cout << endl;
}

/*
Returns the value stored in a cell.  Throws an exception
if bad values are passed.
*/
ValueType board::getCell(int i, int j)
{
   if (i >= 1 && i <= BoardSize && j >= 1 && j <= BoardSize)
      return value[i][j];
   else
      throw rangeError("bad value in getCell");
}

/*
Returns true if cell i,j is blank, and false otherwise.
*/
bool board::isBlank(int i, int j)
{
   if (i < 1 || i > BoardSize || j < 1 || j > BoardSize)
      throw rangeError("bad value in setCell");

   return (getCell(i,j) == Blank);
}

/*
Adds a specific cell value to a target location and updates conflict vectors
*/
void board::addCellVal(int i, int j, int val)
{
	// Do not add a value to the cell if it is blank
    if(!isBlank(i, j))
        return;

	// Set the correct coordinates in value to the input value
    value[i][j] = val;

    // Set the row, col, and square vector conflicts of the cell value to true
    row[i][val] = true;
    col[j][val] = true;
    int squareNum = squareNumber(i, j);
    square[squareNum][val] = true;
}

/*
Remove a value from a cell at a specific coordinate and update conflict vectors
*/
void board::removeCellVal(int i, int j)
{
	// Get the cell value
    int val = getCell(i, j);

    // Stop if the cell value is not a valid number
    if (val < 0)
    	return;

    // Set the row, col, and square vector conflicts of the cell value to false
    row[i][val] = false;
    col[j][val] = false;
    int squareNum = squareNumber(i, j);
    square[squareNum][val] = false;

	// Set the coordinate in the value vector to blank
    value[i][j] = Blank;
}

/*
Prints the current board
*/
void board::print()
{
   for (int i = 1; i <= BoardSize; i++)
   {
      if ((i-1) % SquareSize == 0)
      {
         cout << " -";
	 for (int j = 1; j <= BoardSize; j++)
	    cout << "---";
         cout << "-";
	 cout << endl;
      }
      for (int j = 1; j <= BoardSize; j++)
      {
	 if ((j-1) % SquareSize == 0)
	    cout << "|";
	 if (!isBlank(i,j))
	    cout << " " << getCell(i,j) << " ";
	 else
	    cout << "   ";
      }
      cout << "|";
      cout << endl;
   }

   cout << " -";
   for (int j = 1; j <= BoardSize; j++)
      cout << "---";
   cout << "-";
   cout << endl;
}

/*
Prints the row, column, and square conflict arrays in a grid format
*/
void board::printConflicts()
{
    cout << "row conflicts:" << endl;
    cout << "    1 2 3 4 5 6 7 8 9" << endl;
    for(int i = 1; i <= BoardSize; i++)
    {
        cout << i << "   ";
        for(int s = 1; s <= BoardSize; s++)
        {
            if (row[i][s])
                cout << "T ";
            else
                cout << "F ";
        }
        cout << endl;
    }

    cout << "col conflicts:" << endl;
    cout << "    1 2 3 4 5 6 7 8 9" << endl;
    for(int i = 1; i <= BoardSize; i++)
    {
        cout << i << "   ";
        for(int s = 1; s <= BoardSize; s++)
        {
            if (col[i][s])
                cout << "T ";
            else
                cout << "F ";
        }
        cout << endl;
    }

    cout << "square conflicts:" << endl;
    cout << "    1 2 3 4 5 6 7 8 9" << endl;
    for(int i = 1; i <= BoardSize; i++)
    {
        cout << i << "   ";
        for(int s = 1; s <= BoardSize; s++)
        {
            if (square[i][s])
                cout << "T ";
            else
                cout << "F ";
        }
        cout << endl;
    }
}

/*
Return boolean whether the board is solved
*/
bool board::checkSolved()
{

	// Loop through all cells in board
    for(int i = 1; i <= BoardSize; i++)
    {
        for(int j = 1; j <= BoardSize; j++)
        {
        	// If any cell is blank, the board is not solved
            if (isBlank(i, j))
                return false;
        }
    }

    // If every cell isn't blank, the board is solved
    return true;
}

/*
Calculates a priority score for a given cell with row i and col j
Higher score = higher priority
Score based on fewest number of cell digits that can fit in cell
Ties broken by fewest constraints in a direction (will lower number of future digit options for most possible cells)
*/
int board::priorityCalc(int i, int j)
{
	// Initialize constraints and number of free cells for each constraint type 
	int rowFree = 0;
	int colFree = 0;
	int squareFree = 0;
	int constraints = 0;
	
	// Loop through every digit
	for (int digit = 1; digit <= BoardSize; digit++)
	{
		// Increment constraints if the digit can't be placed due to row, column, or square conflicts
		if (row[i][digit] || col[j][digit] || square[squareNumber(i, j)][digit])
			 constraints += 1;
			 
		// Increment rowFree, colFree, or squareFree if the digit is allowed to be placed by any of the conflict matrices
		if (!row[i][digit])
			rowFree += 1;
		if (!col[j][digit])
			colFree += 1;
		if (!square[squareNumber(i, j)][digit])
			squareFree += 1;
	}
	
	// Return number of constraints * weight factor + maximum number of free squares
	// Makes number constraints more important than free squares, but free squares still a factor in priority (especially for ties)
	return CONSTRAINT_WEIGHT * constraints + max(max(rowFree, colFree), squareFree);
}

/*
findNextCell intelligently finds a new candidate blank cell
by finding the highest priority blank cell on the board
(the cell with the least number of valid choices)
it returns a vector that gives the position of this candidate cell
*/
vector<int> board::findNextCell()
{
	// Variable initializations
	int priority = 0;
	int maxPriority = 0;
	vector<int> nextCell(2, 0);

	// Loop through entire board
	for (int i = 1; i <= BoardSize; i++)
	{
		for (int j = 1; j <= BoardSize; j++)
		{
			if (isBlank(i, j))
				priority = priorityCalc(i, j); // Call priorityCalc on every blank cell in the board
			else
				priority = -1; // Never return a non-blank cell

			if (priority > maxPriority) // If this candidate is higher priority than last candidate
			{
				nextCell.at(0) = i; // Assign positions to vector
				nextCell.at(1) = j;
				maxPriority = priority; // Update maxPriority
			}
		}
	}
	return nextCell; // Return next target cell candidate
}

/*
Solve recursively places a new candidate digit in a cell with priority calculations if the board isn't solved
Checks whether the placed cell results in 1 more digit, position in a row, position in a column, or position in a square that can be filled.
Solves the board given the new placed cell(s)
Backtracks to remove all cells placed once the new board has been attempted to be solved
*/
void board::solve()
{
	currentCalls++; // increase number of recursive calls each time solve is called
	if (checkSolved() && numSolutions < 1) //stop and print if first correct solution.
	{
		print();
		numSolutions++;
	}
	
	else if (numSolutions < 1) // Stop recursion if first solution found
	{
		vector<int> nextCell = findNextCell(); // Positional vector of next candidate
		for (int i = 1; i <= BoardSize; i++) // Loop through all numbers that could be placed
		{
			if (!row[nextCell.at(0)][i] &&
			 !col[nextCell.at(1)][i] &&
			 !square[squareNumber(nextCell.at(0), nextCell.at(1))][i]) //if no conflicts for this placement
			 {
				vector<vector<int> > cellsAdded; // Initialize a vector of cell coordinates added in this iteration
			 	bool hasConflicts = false; // Initialize a conflict boolean
			 	
			 	// Add the next target cell to fill to the board
			 	addCellVal(nextCell.at(0), nextCell.at(1), i);
			 	
			 	// Add the next target cell to fill to the list of cells added
			 	vector<int> addVec;
			 	addVec.push_back(nextCell.at(0));
			 	addVec.push_back(nextCell.at(1));
			 	cellsAdded.push_back(addVec);
			 	
			 	// Check whether the placed cell results in 1 more digit, position in a row, position in a column, or position in a square that 
				//		can be filled. Fills the last spot if possible
				// Stores whether completing 1 last spot will cause any conflicts
				hasConflicts = 	completeDigit(i, cellsAdded) ||
			 					completeLastArray(ROW, nextCell.at(0), cellsAdded) ||
			 					completeLastArray(COLUMN, nextCell.at(1), cellsAdded) ||
								completeLastArray(SQUARE, squareNumber(nextCell.at(0), nextCell.at(1)), cellsAdded);
				
				// If there are no visible conflicts, keep solving the board
				if (!hasConflicts)
			 		solve();

				// Remove all cells that were added during this iteration of solve (all stored in cellsAdded)
				for (int index = 0; index < cellsAdded.size(); index++)
			 	{
					removeCellVal(cellsAdded[index][0], cellsAdded[index][1]);
				}

			 }
		}
	}
}

/*
Checks if a given digit has only 1 digit left to be placed on the board
If there is only 1 digit left, it is placed on the board
Checks whether adding that digit will open up opportunities to complete any rows, columns, or squares
Returns whether conflicts could arise by placing digit
*/
bool board::completeDigit(int digit, vector<vector<int> > &cellsAdded)
{
	// Initialize a digit count, row replace num, column replace num, and coordinate vector
	int count = 0;
	int replaceRow = 0;
	int replaceCol = 0;

	// Loop through every row in the row conflict matrix
	for (int rowPos = 1; rowPos <= BoardSize; rowPos++)
	{
		// If the digit at the current row can't be placed, add 1 to count
		if (row[rowPos][digit])
			count++;
		// Otherwise (digit can be placed in row), record the current row position
		else
			replaceRow = rowPos;
	}

	// Only continue if there is 1 digit left to be filled
	// A single remaining digit can be placed because it can only go in 1 place
	// Return false because there can't be new conflicts if no digits were added to the board
	if (count != BoardSize - 1)
		return false;

	// By reaching this point, only 1 digit remains to place

	// Loop through every column in the column conflict matrix
	for (int colPos = 1; colPos <= BoardSize; colPos++)
	{
		// If the digit at the current column can be placed, record the column
		if (!col[colPos][digit])
			replaceCol = colPos;
	}

	// Add to the board the given digit at coordinates (replaceRow, replaceCol)
	addCellVal(replaceRow, replaceCol, digit);
	
	// Add the coordinates filled to the parameter vector cellsAdded
	vector<int> addVec;
	addVec.push_back(replaceRow);
	addVec.push_back(replaceCol);
	cellsAdded.push_back(addVec);

	// Check if the added digit can allow a row, column, or square to be completed
	// Return whether any of those additions would cause future conflicts
	return completeLastArray(ROW, replaceRow, cellsAdded) || completeLastArray(COLUMN, replaceCol, cellsAdded) || completeLastArray(SQUARE, squareNumber(replaceRow, replaceCol), cellsAdded);
}

/*
Checks if there is only 1 option left in the constraintType #num (ex. row #5, square #9) and whether it would cause a conflict
If there isn't only 1 option, stop
If there is only 1 option, check whether adding the option it would cause conflicts.
Check to see if adding the option would cause only one more possible instance of a digit by calling completeDigit
Return bool whether there is a conflict from completing the constraintType
*/
bool board::completeLastArray(int constraintType, int num, vector<vector<int> > &cellsAdded)
{
	// Initialize a count and row, column, and digit of replacement
	int count = 0;
	int replaceDig = 0;
	int replaceRow = 0;
	int replaceCol = 0;
	
	// Loop through every element in the specified constraintType
	for (int i = 1; i <= BoardSize; i++)
	{
		// Change specific numbers based on the constraintType
		// Increment counter if there is a non-blank cell in constraintType num
		// Otherwise, record the current row and column as possible coordinates to replace
				
		if (constraintType == ROW) 
		{
			if (!isBlank(num, i))
			{
				count++;
			} 
			else 
			{
				replaceRow = num;
				replaceCol = i;
			}
		}
		else if (constraintType == COLUMN)
		{
			if (!isBlank(i, num))
			{
				count++;
			} 
			else 
			{
				replaceRow = i;
				replaceCol = num;
			}
		}
		else if (constraintType == SQUARE)
		{
			if (!isBlank(rowOfSquare(num, i), colOfSquare(num, i)))
			{
				count++;
			} 
			else 
			{
				replaceCol = colOfSquare(num, i);
				replaceRow = rowOfSquare(num, i);
			}
		}
		
	}
	
	// If there isn't only 1 blank cell, don't continue
	// A single blank cell can be filled because only option should exist to fill it
	// Return false because there can't be new conflicts if no cells were filled
	if (count != BoardSize - 1)
		return false;
	
	// By reaching here, there is only 1 blank cell in constraintType num, so it can be filled
	
	// Loop through every digit in the constraintType num conflict vector
	for (int digit = 1; digit <= BoardSize; digit++)
	{
		
		// Change specific numbers based on constraintType
		// If the digit in the appropriate conflict vector does not have a conflict,
		//		Record the digit as the one to place at the coordinates found earlier
		
		if (constraintType == ROW) 
		{
			if (!row[replaceRow][digit])
				replaceDig = digit;
		}
		else if (constraintType == COLUMN) 
		{
			if (!col[replaceCol][digit])
				replaceDig = digit;
		}
		else if (constraintType == SQUARE) 
		{
			if (!square[squareNumber(replaceRow, replaceCol)][digit])
				replaceDig = digit;
		}	
	}
	
	// If placing replaceDigit at coordinates (replaceRow, replaceCol) would cause any conflicts
	//		Return true from the function to indicate a conflict
	if (row[replaceRow][replaceDig] ||
		col[replaceCol][replaceDig] ||
		square[squareNumber(replaceRow, replaceCol)][replaceDig])
		return true;
	
	// By reaching here, placing the replaceDigit at its appropriate coordinates would not cause conflicts
	
	// Add replaceDig at coordinates (replaceRow, replaceCol) to the board
	addCellVal(replaceRow, replaceCol, replaceDig);
	
	// Add the coordinates filled to the parameter vector cellsAdded
	vector<int> addVec;
	addVec.push_back(replaceRow);
	addVec.push_back(replaceCol);
	cellsAdded.push_back(addVec);
	
	// Check if the digit just placed would allow for a digit completion
	// Return whether adding that digit would cause a future conflict
	return completeDigit(replaceDig, cellsAdded);
}

