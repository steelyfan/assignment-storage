// Alex Free, Benjamin Fox -- Project 4b
// Board class function declarations

#ifndef BOARD_H
#define BOARD_H

typedef int ValueType; // The type of the value in a cell
const int Blank = -1;  // Indicates that a cell is blank

const int SquareSize = 3;  //  The number of cells in a small square
                           //  (usually 3).  The board has
                           //  SquareSize^2 rows and SquareSize^2
                           //  columns.
                           
// constraintType integer constants
const int ROW = 0;
const int COLUMN = 1;
const int SQUARE = 2;

// Weight of constraints relative to free cells in priorityCalc
const int CONSTRAINT_WEIGHT = 5;

const int BoardSize = SquareSize * SquareSize;

const int MinValue = 1;
const int MaxValue = 9;
extern int currentCalls;
extern int totalCalls;
extern int boardNumber;

extern int numSolutions;
int squareNumber(int i, int j);

class board
// Stores the entire Sudoku board
{
   public:
      board(int);
      void clear();
      void initialize(ifstream &fin);
      void initializeConflicts();
      void print();
      void printConflicts();
      void addCellVal(int i, int j, int val);
      void removeCellVal(int i, int j);
      bool checkSolved();
      bool isBlank(int, int);
      void solve();
      vector<int> findNextCell();
      int priorityCalc(int i, int j);

      ValueType getCell(int, int);
      bool completeDigit(int digit, vector<vector<int> > &cellsAdded);
      bool completeLastArray(int constraintType, int num, vector<vector<int> > &cellsAdded);

   private:

      // The following matrices go from 1 to BoardSize in each
      // dimension, i.e., they are each (BoardSize+1) * (BoardSize+1)

      matrix<ValueType> value;
      matrix<bool> row;
      matrix<bool> col;
      matrix<bool> square;
};

#endif
