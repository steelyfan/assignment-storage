//Alexander Free, Benjamin Fox -- Project 2a
//This is the header file for the card class
//It described the card class and its members

#ifndef DECK_H
#define DECK_H

#include "card.h"
#include "d_node.h"

class deck
{
    public:
    deck();
    deck(node<card>* p);
    friend ostream& operator<< (ostream& ostr, deck& d);
    void shuffle();
    node<card>* findCard(int position);
    ~deck();
    node<card>* deal();
    void replace(node<card>* c);

    private:
    node<card>* front;
};

#endif
