//Alexander Free, Benjamin Fox -- Project 2a
//This is the header file for the card class
//It described the card class and its members

#ifndef CARD_H
#define CARD_H

#include <iostream>

using namespace std;

class card
{
    public:
    card(int val, int s);
    card(const card &c);
    card& operator= (const card &c);
    void setValue(int val);
    void setSuit(int s);
    void setChosen(bool c);
    int getValue();
    int getSuit();
    bool getChosen();
    friend ostream& operator<< (ostream& ostr, card& c);

    private:
    int value;
    int suit;
    bool chosen;
};

#endif
