//Alexander Free, Benjamin Fox -- Project 2a
//This is the source file for the deck class.
//It contains a constructor, functions to shuffle and
//find a card at a given index, and an overloaded
//<< operator to print the details of the deck

#include "deck.h"
#include <stdlib.h>
#include <time.h>

/*
Creates a deck with linked list of cards in sorted order A-K, Clubs-Diamonds-Hearts-Spades
*/
deck::deck()
{
    for(int i = 3; i >= 0; i--)
    {
        for (int j = 12; j >= 0; j--)
        {
            card c(j, i);
            node<card>* newNode;
            if (front != NULL)
                newNode = new node<card> (c, front);
            else
                newNode = new node<card> (c, NULL);
            front = newNode;
        }

    }
}

deck::deck(node<card> *p)
{
    front = p;
}

deck::~deck()
{
    while (front != NULL)
    {
        node<card>* p = front;
        front = front->next;
        delete p;
    }
}

/*
Overloaded << prints out all the cards in the deck
*/
ostream& operator<< (ostream& ostr, deck& d)
{
    node<card>* curr;
    curr = d.front;
    while(curr != NULL)
    {
        ostr << curr->nodeValue;
        curr = curr->next;
    }
    return ostr;
}

/*
Randomly shuffles the deck by swapping two random cards 1000 times
*/
void deck::shuffle()
{
    srand(time(NULL));
    for(int i = 0; i < 1000; i++)
    {
        int randInt = rand() % 52;
        int randInt2 = rand() % 52;
        node<card>* node_temp = findCard(randInt);
        node<card>* node_temp2 = findCard(randInt2);
        card c = node_temp->nodeValue;
        node_temp->nodeValue = node_temp2->nodeValue;
        node_temp2->nodeValue = c;
    }
}

/*
Finds the card in the deck at a given index
*/
node<card>* deck::findCard(int position)
{
    node<card>* curr = front;
    for(int i = 0; i < position; i++)
    {
        curr = curr->next;
    }
    return curr;
}

node<card>* deck::deal()
{
    if (front != NULL)
    {
        node<card> *p = front;
        front = front->next;
        p->next = NULL;
        return p;
    }
    return NULL;
}

void deck::replace(node<card>* c)
{
    node<card> *curr = front;
    while (curr->next != NULL)
    {
        curr = curr->next;
    }
    curr->next = c;
}

