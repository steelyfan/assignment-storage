//Alexander Free, Benjamin Fox -- Project 2a
//This is the source file for the card class.
//It contains a constructor, functions to get and set
//the three private fields (card, suit, and chosen), and
//an overloaded << operator to print the details of a card

#include "card.h"
#include <string>

using namespace std;

/*
Creates a card with a given suit and value
*/
card::card(int val, int s)
{
    value = val;
    suit = s;
    chosen = false;
}

card::card(const card &c)
{
    value = c.value;
    suit = c.suit;
    chosen = c.chosen;
}

card& card::operator= (const card &c)
{
    value = c.value;
    suit = c.suit;
    chosen = c.chosen;
    return *this;
}

/*
Sets the card value of a card
*/
void card::setValue(int val)
{
    value = val;
}


/*
Sets the suit of a card
*/
void card::setSuit(int s)
{
    suit = s;
}

/*
Sets whether a card has been chosen or not
*/
void card::setChosen(bool c)
{
    chosen = c;
}

/*
Returns the value of the card
*/
int card::getValue()
{
    return value;
}

/*
Returns the suit of the card
*/
int card::getSuit()
{
    return suit;
}

/*
Returns whether the card has been chosen yet
*/
bool card::getChosen()
{
    return chosen;
}

/*
Overloaded << prints out the card in form "val of suit"
*/
ostream& operator<< (ostream& ostr, card& c)
{
    string valStringArr[13] = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
    string suitStringArr[4] = {"Clubs", "Diamonds", "Hearts", "Spades"};
    ostr << valStringArr[c.value] << " of " << suitStringArr[c.suit] << endl;
    return ostr;
}
