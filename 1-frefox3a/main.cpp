// Alex Free, Benjamin Fox -- Project 3b
// Main file
// Contains the main function, as well as the functions findWords and findMatches
// The main function calls findMatches, which calls findWords for each of the "directions"

#include "grid.h"
#include "dictionary.h"



/*
*findWords takes in a dictionary, a grid, amd integers which give the initial y and x position
*and the "direction" (+1/-1) that the positions will be incremented in.
*it forms an initial 5-character word, and then enters a loop where it searches the dictionary
*for the potential word, prints it if found, and then adds another character to the candidate word.
*//
void findWords(const dictionary &d, const grid &g, int yPos, int xPos, int yInc, int xInc)
{
    //declares the candidate word
    string word;

    //fprm the initial 5-character candidate
    for(int i = 0; i < 5; i++)
    {
        //gets the starting position
        int y = (yPos + yInc*i)%g.getRows();
        int x = (xPos + xInc*i)%g.getColumns();

        //wrap around
        if (y < 0)
            y += g.getRows();

        if (x < 0)
            x += g.getColumns();

        //add the character to candidate word
        word += g.getValue(y, x);
    }

    for(int i = 5; i < g.getRows(); i++){
        //search the dictionary for the candidate word
        int index = d.search(word, 0, d.getLength() - 1);

        //if the word exists print the word, it's index,
        //and the position of it's starting character in the grid.
       if (index != -1)
       {
            cout << word << " at " << index << " in dictionary, and (" << yPos << ", " << xPos << ") in grid" << endl;
       }
        //get starting position
        int y = (yPos + yInc*i)%g.getRows();
        int x = (xPos + xInc*i)%g.getColumns();

        //wrap around
        if (y < 0)
            y += g.getRows();

        if (x < 0)
            x += g.getColumns();
        //add next character
        word += g.getValue(y, x);
    }
}

/*
*findMatches takes in a dictionary and a grid object,
*it calls findWords for each of the possible starting characters
*and directions in the list
*/
void findMatches(const dictionary &d, const grid &g)
{
    //account for every position in the grid
    for (int y = 0; y < g.getRows(); y++)
    {
        for (int x = 0; x < g.getColumns(); x++)
        {
            //account for all 8 directions
            for(int yInc = -1; yInc <= 1; yInc++)
            {
                for(int xInc = -1; xInc <= 1; xInc++)
                {
                    //ignore the case where both are zero (not a direction)
                    if(xInc == 0 && yInc == 0)
                        continue;

                    findWords(d, g, y, x, yInc, xInc);
                }
            }
        }
    }
}

/*
*search reads the filename from the keyboard, passes it to the grid constructor,
*initializes and sorts a dictionary object, and then passes these objects to findMatches
*
*/
void search()
{
    string file;
    cout << "Enter the grid filename" << endl;
    cin >> file;

    grid g(file);
    dictionary d;
    cout << "Sorting dictionary, please wait..." << endl;
    d.sort();

    findMatches(d, g);
}

/*
*main simply calls search
*/
int main()
{
    search();
}
