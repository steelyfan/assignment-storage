// Alex Free, Benjamin Fox -- Project 3b
// Dictionary class implementation
// Contains functions to read, sort, and search the dictionary
// Also contains overloaded print operator and getLength function

#include "dictionary.h"

/*
Dictionary constructor reads dictionary words
*/
dictionary::dictionary()
{
    readWords();
}

/*
Opens dictionary.txt and reads words into a vector
*/
void dictionary::readWords()
{
    ifstream fileIn;
    fileIn.open("dictionary.txt");
    if(!fileIn)
    {
        cerr << "The dictionary file does not exit" << endl;
        exit(1);
    }

    string word;
    while(fileIn >> word)
    {
        dict.push_back(word);
    }
    fileIn.close();
}

/*
Prints whole dictionary, one word per line
*/
ostream& operator<< (ostream& ostr, const dictionary& d)
{
    for (int i = 0; i < d.dict.size(); i++)
    {
        ostr << d.dict.at(i) << endl;
    }
    return ostr;
}

/*
Uses selection sort to sort dictionary vector
*/
void dictionary::sort()
{
    // Repeat sort for 
	for (int i = 0; i < dict.size() - 1; i++)
    {
    	// Print sorting progress for user
        if(i % 1000 == 0)
            cout << i << "/" << dict.size() << endl;

		int minValIndex = i;

		 // Find next alphabetical value in dictionary 
        for (int j = i+1; j < dict.size(); j++)
        {
            if (dict.at(j).compare(dict.at(minValIndex)) < 0)
            {
                minValIndex = j;
            }
        }
        
    	// Swap minVal with current value at i 
        if (minValIndex != i)
        {
            string tempString = dict.at(i);
            dict.at(i) = dict.at(minValIndex);
            dict.at(minValIndex) = tempString;
        }
    }
}

/*
Use binary search to find key in sorted dictionary
*/
int dictionary::search(string key, int lowBound, int upBound) const
{
	
    int mid = (upBound + lowBound)/2; // Find mid value

    if (lowBound > upBound)	// If the lower bound and upper bound have swapped, return -1 (not found)
        return -1;

	// Either return mid or search appropriate half of remaining dictionary
    if (key.compare(dict.at(mid)) == 0)
        return mid;
    else if (key.compare(dict.at(mid)) < 0)
        return search(key, lowBound, mid - 1);
    else if (key.compare(dict.at(mid)) > 0)
        return search(key, mid + 1, upBound);

    return -1;
}

/*
Return the length of the dictionary vector (number of words)
*/
int dictionary::getLength() const
{
    return dict.size();
}

