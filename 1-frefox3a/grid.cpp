// Alex Free, Benjamin Fox -- Project 3b
// Grid class implementation
// Contains functions to get rows, columns, and grid values
// Constructor is given a file to make a grid from

#include "grid.h"

/*
Constructor opens file, reads size, and creates matrix
*/
grid::grid(string filename)
{
    ifstream fileIn;
    fileIn.open(filename.c_str());
    if(!fileIn)
    {
        cerr << "The matrix file does not exit" << endl;
        exit(1);
    }

    int rows, columns;
    fileIn >> rows;
    fileIn >> columns;

    m.resize(rows, columns);

    for (int y = 0; y < rows; y++)
    {
        for (int x = 0; x < columns; x++)
        {
            fileIn >> m[y][x];
        }
    }
}

/*
Get number of rows in matrix
*/
int grid::getRows() const
{
    return m.rows();
}

/*
Get number of columns in matrix
*/
int grid::getColumns() const
{
    return m.cols();
}

/*
Get value at index (y, x) in matrix
*/
char grid::getValue(int y, int x) const
{
    return m[y][x];
}
