// Alex Free, Benjamin Fox -- Project 3b
// Header file for grid contains all function declarations and member variables

#ifndef GRID_H
#define GRID_H

#include <fstream>
#include <string>
#include "d_matrix.h"

class grid
{
    public:
        grid(string filename);
        int getRows() const;
        int getColumns() const;
        char getValue (int y, int x) const;

    private:
        matrix<char> m;

};

#endif
